$( document ).ready(function() {
	function upload_img_prev (e) {
      var selectedFile = $('#upload').get(0).files[0];
	  dataUpload = new FormData();
	  if (selectedFile != undefined){
		  dataUpload.append('file',selectedFile);
		  img_width = $( "#img_width" ).val();
		  number_of_color = $( "#number_of_color" ).val();
		  palette = $( "#palette" ).val();
		  dataUpload.append('img_width',img_width);
		  dataUpload.append('number_of_color',number_of_color);
		  dataUpload.append('dithering_name',$('#dithering').val());
		  dataUpload.append('palette',palette);
		  $('#load').css('display','block');
		  $.ajax({
					type: 'POST',
					data:dataUpload,
					url: 'api/preview',
					contentType: false,
					cache: false,
					processData: false,
					success: function(data){
						if (data['msg'] != 'error'){
							$('#preview_img').attr('src',data['img_path'] + '/' + new Date().getTime());
							$('#left_col').css('display','inline-block');
							$('#dowload').css('display','inline-block');
							$('#load').css('display','none');
							$('#img_wrapper').css('width','');
						}else{
							$('#load').css('display','none');
							show_msg('Что - то пошло не так');
						}
						
					},
				});
	  }
    }
	
	function upload_img_schema (e) {
      var selectedFile = $('#upload').get(0).files[0];
	  dataUpload = new FormData();
	  if (selectedFile != undefined){
		  dataUpload.append('file',selectedFile);
		  img_width = $( "#img_width" ).val();
		  number_of_color = $( "#number_of_color" ).val();
		  palette = $( "#palette" ).val();
		  dataUpload.append('img_width',img_width);
		  dataUpload.append('number_of_color',number_of_color);
		  dataUpload.append('dithering_name',$('#dithering').val());
		  dataUpload.append('palette',palette);
		  $('#load').css('display','block');
		  $.ajax({
					type: 'POST',
					data:dataUpload,
					url: 'api/schema',
					contentType: false,
					cache: false,
					processData: false,
					success: function(data){
						if (data['msg'] != 'error'){
							document.location = 'http://192.168.0.103:5000/api/schema_download/' + data['schema_name'];
							$('#load').css('display','none');
						}else{
							$('#load').css('display','none');
							show_msg('Что - то пошло не так');
						}
					},
				});
	  }
    }
	
	function send_comment (e) {
      var selectedFile = $('#upload').get(0).files[0];
	  comment = $( "#comment" ).val();
	  if (comment != 'Сообщение отправлено. Спасибо за отзыв!'){
		  dataUpload = new FormData();
		  dataUpload.append('comment',comment);
		  $( "#comment" ).val('Сообщение отправлено. Спасибо за отзыв!')
		  $.ajax({
					type: 'POST',
					data:dataUpload,
					url: 'api/send_comment',
					contentType: false,
					cache: false,
					processData: false,
					success: function(data){
						
					},
				});
	  }
    }
	
	function show_msg(text){
		$('#msg_background').css('display','block');
		$('#msg_box').css('display','inline-block');
		$('#msg').val(text);
	}
	
	function close_msg(){
		$('#msg_background').css('display','none');
		$('#msg_box').css('display','none');
	}
	$('#upload').on('input', upload_img_prev);
	$('#accept').on('click', upload_img_prev);
	$('#dowload').on('click', upload_img_schema);
	$('#send_comment').on('click', send_comment);
	$('#comment').on('click', function(){
		comment = $( "#comment" ).val();
		if ($( "#comment" ).val() == 'Сообщение отправлено. Спасибо за отзыв!'){
			$( "#comment" ).val('')
		}
	});
	$('#msg_ok').on('click',close_msg);
});