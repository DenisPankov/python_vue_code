import requests, os
from bs4 import BeautifulSoup
from time import sleep

class coss_stitch_bot_handler:
    def __init__(self):
        f = open(os.path.join(os.path.dirname(__file__),'proxies.txt'))
        self.proxies = []
        for line in f:
            self.proxies.append(line.rstrip("\n"))
        f.close()
    def send_message(self,message:str):
        for proxy in self.proxies:
            try:
                response = requests.get('https://api.telegram.org/botxxxxxxxxxxxxxxxxxxxxxxxxxxx' + message,proxies={'https':proxy})
                return 200
            except:
                print('next proxy')
                sleep(0.01)
        print('proxy end')

class bot_keeper: 
    bot = coss_stitch_bot_handler()
    @staticmethod
    def get_bot():
        if bot_keeper.bot == None:
           bot_keeper.bot = coss_stitch_bot_handler()
        return bot_keeper.bot

