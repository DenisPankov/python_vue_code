import requests,json
from bs4 import BeautifulSoup
from PIL import ImageColor


headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36',
        }
html_code = requests.get("https://firma-gamma.ru/articles/colormap-muline/gamma/",headers=headers)
soup = BeautifulSoup(html_code.text, 'html.parser')
html = soup.select("tr")


f = open('palette_gamma.txt', 'w')
for i in range(1,len(html)):
    f.write(html[i].findAll('td')[0].text +
            ';' +
            str(ImageColor.getrgb(html[i].findAll('td')[4]['style'].split(';')[1].split(':')[1])) + '\n')
f.close()
