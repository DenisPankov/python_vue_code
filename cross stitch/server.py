from flask import Flask, current_app,jsonify, request
from cross_stitch.cross_stitch import CrossStitchSchema
from cross_stitch_bot.cross_stitch_bot import bot_keeper
import uuid
import os
import zipfile
import time
import threading
import shutil

app = Flask(__name__)

@app.route('/')
def index() -> str:
    return current_app.send_static_file('html/index.html')

@app.route('/api/send_comment',methods=['POST'])
def send_comment():
    bot_handler = bot_keeper.get_bot()
    bot_handler.send_message(request.form.to_dict()['comment'])
    response = {'status':200}
    return jsonify(response)

@app.route('/favicon.ico')
def get_fivecon() -> str:
    return current_app.send_static_file('img/favicon.ico')

@app.route('/img/preview/<img_path>/<time>')
def get_preview(img_path,time) -> str:
    return current_app.send_static_file('img/preview/'+ img_path)

@app.route('/img/<img_path>/<time>')
def get_img(img_path,time) -> str:
    return current_app.send_static_file('img/'+ img_path)

@app.route('/api/schema_download/<schema_name>')
def get_schema(schema_name) -> str:
    print('img/schema/'+ schema_name + '/archive.zip')
    return current_app.send_static_file('img/schema/'+ schema_name + '/archive.zip')

@app.route('/api/preview', methods=['POST'])
def create_preview() -> str:
    try:
        img = request.files['file']
        post_data = request.form.to_dict()
        path = 'static/img/preview'
        uid_source = str(uuid.uuid4())
        img.save(path + '/' + uid_source + '.png')
        schema = CrossStitchSchema()
        schema.import_img(path + '/' + uid_source + '.png')
        uid = str(uuid.uuid4())
        
        schema.create_schema(int(post_data['img_width']),int(post_data['number_of_color']),post_data['dithering_name'],post_data['palette'])
        schema.export_img_previwe('static/img/preview/' + uid +'.png')
        filename = 'img/preview/' + uid +'.png'
        response = {'img_path':filename}
        return jsonify(response)
    except:
        response = {'msg':'error'}
        return jsonify(response)

@app.route('/api/schema', methods=['POST'])
def create_schema() -> str:
    try:
        img = request.files['file']
        post_data = request.form.to_dict()

        schema_name = str(uuid.uuid4())
        path = 'static/img/schema/' + schema_name
        os.mkdir(path)
        
        img.save(path + '/source.png')
        schema = CrossStitchSchema()
        schema.import_img(path + '/source.png')
        schema.create_schema(int(post_data['img_width']),int(post_data['number_of_color']),post_data['dithering_name'],post_data['palette'])
        schema.export_img(path + '/')

        newzip=zipfile.ZipFile(path + '/archive.zip','w')
        newzip.write(path + '/result.png', 'result.png')
        newzip.write(path + '/result_icons.png', 'result_icons.png')
        newzip.write(path + '/result_icons_color.png','result_icons_color.png')
        newzip.write(path + '/result_img_list.png', 'result_img_list.png')
        newzip.write(path + '/source.png','source.png')
        newzip.close()

        
        response = {'schema_name':schema_name}
        return jsonify(response)
    except:
        response = {'msg':'error'}
        return jsonify(response)

def clear_dir():
    previews_patch = os.path.join(os.path.dirname(__file__), 'static\img\preview')
    schemas_patch = os.path.join(os.path.dirname(__file__), 'static\img\schema')
    previews_files = os.listdir(previews_patch)
    schemas_dir = os.listdir(schemas_patch)

    for direc in schemas_dir:
        if time.time() - os.path.getctime(os.path.join(schemas_patch,direc)) >= 3600:
            shutil.rmtree(os.path.join(schemas_patch,direc))
            #print(os.path.join(schemas_patch,direc))

    for file in previews_files:
        if time.time() - os.path.getctime(os.path.join(previews_patch,file)) >= 3600:
            os.remove(os.path.join(previews_patch,file))
            #print(os.path.join(previews_patch,file))
    time.sleep(60)

cleaners_thread = threading.Thread(target=clear_dir)
cleaners_thread.daemon = False
cleaners_thread.start()

app.run(debug=True,host='192.168.0.103')

