from cross_stitch import CrossStitchSchema
import time
import cProfile

start_time = time.time()
schema = CrossStitchSchema()
schema.import_img('source.png')
schema.create_schema(150,15,'','')
schema.export_img()

print (time.time() - start_time)
print(str(len(schema.schema)) + ' - ширина')
print(str(len(schema.schema[0])) + ' - высота')
print(str(len(schema.schema) * len(schema.schema[0])) + '- клеток')
