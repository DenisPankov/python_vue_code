from PIL import Image, ImageDraw, ImageFont
import time, copy, os,ast, math


class CrossStitchSchema:
    def __init__(self) -> None:
        def number_new_line(numbers_str:str):
            number_list = numbers_str.split(',')
            new_numbers_str = ''
            for i in range(len(number_list)):
                if i != len(number_list) - 1:
                    new_numbers_str += number_list[i] + ',\n'
                else:
                    new_numbers_str += number_list[i]
            return new_numbers_str
        self.schema = []
        self.all_color_arr = []
        self.palettes = {'DMC': [],
                         'Gamma': [],
                         'Anchor': [],
                         'Madeira': [], }
        gamma_palette = open(os.path.join(os.path.dirname(__file__), 'palette/palette_gamma.txt'))
        for line in gamma_palette:
            row = line.split(';')
            self.palettes['Gamma'].append({'number': number_new_line(row[0]),
                                            'color': ast.literal_eval(row[1],)})

    def import_img(self, path_to_img: str = 'source.png'):
        self.im = Image.open(path_to_img).convert('RGBA')
        self.width, self.heigth = self.im.size
        self.pix_rate = 20
        self.img_offset = 2

    def create_schema(self, new_width, color_quant, dithering_name,palette) -> None:
        print('Begin create_schema')
        new_heigth = round(self.heigth / (self.width / new_width))
        self.im = self.im.resize((new_width, new_heigth))
        self.image_get = self.im.load()
        ##        for i in range(pix_rate,self.width - pix_rate,pix_rate*2):
        ##            point_list = []
        ##            for j in range(pix_rate,self.heigth - pix_rate, pix_rate*2):
        ##                temp_red = 0;
        ##                temp_green = 0;
        ##                temp_blue = 0;
        ##                for x in range(i - pix_rate,i + pix_rate,1):
        ##                    for y in range(j - pix_rate,j + pix_rate,1):
        ##                        color = self.image_get[x,y]
        ##                        temp_red += color[0];
        ##                        temp_green += color[1];
        ##                        temp_blue += color[2];
        ##
        ##                temp_red /= (pix_rate*2)**2;
        ##                temp_green /= (pix_rate*2)**2;
        ##                temp_blue /= (pix_rate*2)**2;
        ##                point_list.append(self.SchemaPoint(round(temp_red),round(temp_green),round(temp_blue),number))
        ##            self.schema.append(point_list)
        for i in range(new_width):
            point_list = []
            for j in range(new_heigth):
                point_list.append(
                    self.SchemaPoint(self.image_get[i, j][0], self.image_get[i, j][1], self.image_get[i, j][2]))
            self.schema.append(point_list)

        self.choosen_palett = palette
        # smooth_step = 2
        self.old_color_schema = copy.deepcopy(self.schema)
        self.median_cut(color_quant)
        if dithering_name != '':
            self.dithering(dithering_name)
            self.median_cut(color_quant)
        ##        while smooth_step <= 40:
        ##            print(smooth_step)
        ##            self.smoothing(smooth_step)
        ##            self.nulling_smoothing()
        ##            smooth_step += 2
        print('End create_schema')

    def median_cut(self, color_quant: int) -> None:
        def get_main_color_and_interval(points: dict):
            red_max = 0
            red_min = 255
            green_max = 0
            green_min = 255
            blue_max = 0
            blue_min = 255
            for point in points:
                if red_max < point.red:
                    red_max = point.red

                if red_min > point.red:
                    red_min = point.red

                if green_max < point.green:
                    green_max = point.green

                if green_min > point.green:
                    green_min = point.green

                if blue_max < point.blue:
                    blue_max = point.blue

                if blue_min > point.blue:
                    blue_min = point.blue
            red_interval = red_max - red_min
            green_interval = green_max - green_min
            blue_interval = blue_max - blue_min
            if green_interval < red_interval > blue_interval:
                interval = red_interval
                main_color = 'red'
            elif red_interval < green_interval > blue_interval:
                interval = green_interval
                main_color = 'green'
            else:
                interval = blue_interval
                main_color = 'blue'
            return {'main_color': main_color,
                    'interval': interval}

        def average_color(points: list):
            temp_red = 0
            temp_green = 0
            temp_blue = 0
            for point in points:
                temp_red += point.red
                temp_green += point.green
                temp_blue += point.blue
            temp_red = round(temp_red / len(points))
            temp_green = round(temp_green / len(points))
            temp_blue = round(temp_blue / len(points))

            if self.choosen_palett != '':
                palett_color = find_nearest_color((temp_red,temp_green,temp_blue))
                temp_red = palett_color[0]
                temp_green = palett_color[1]
                temp_blue = palett_color[2]

            for point in points:
                point.red = temp_red
                point.green = temp_green
                point.blue = temp_blue
            return (temp_red, temp_green, temp_blue)

        boxes = []
        points = []

        for row in self.schema:
            for cell in row:
                points.append(cell)

        def find_nearest_color(color:tuple) -> tuple:
            current_palett = self.palettes[self.choosen_palett]
            diff = 99999
            for color_palette in current_palett:
                diff_temp = abs(color[0] - int(color_palette['color'][0])) + abs(color[1] - int(color_palette['color'][1])) + abs(color[2] - int(color_palette['color'][2]))
                if diff_temp < diff:
                    nearest_color = color_palette['color']
                    diff = diff_temp
            return nearest_color


        box_info = get_main_color_and_interval(points)
        boxes.append({'points': points,
                      'interval': box_info['interval'],
                      'main_color': box_info['main_color']})
        while len(boxes) < color_quant:
            max_box = {'points': None,
                       'interval': -1,
                       'main_color': None}
            for box in boxes:
                sorted(box['points'], key=lambda points: points.get_color(box['main_color']))
                if max_box['interval'] < box['interval']:
                    max_box = box
            new_box = copy.copy(max_box)

            index_for_delet = []
            for i in range(len(boxes)):
                if boxes[i] == max_box:
                    index_for_delet.append(i)

            for index in index_for_delet:
                del boxes[index]

            color_list = [point.get_color(new_box['main_color']) for point in new_box['points']]
            median = max(color_list) - round(new_box['interval'] / 2)
            arr_of_points_arr = [[], []]
            for point in new_box['points']:
                if point.get_color(new_box['main_color']) >= median:
                    arr_of_points_arr[1].append(point)
                else:
                    arr_of_points_arr[0].append(point)
            for point_arr in arr_of_points_arr:
                box_info = get_main_color_and_interval(point_arr)
                boxes.append({'points': point_arr,
                              'interval': box_info['interval'],
                              'main_color': box_info['main_color']})
        self.colors_arr = []
        for box in boxes:
            if len(box['points']):
                self.colors_arr.append(average_color(box['points']))

        self.color_img_arr = []
        self.color_img_arr_hr = []
        path_to_icons = os.path.join(os.path.dirname(__file__), 'icons')
        files = os.listdir(path_to_icons)
        for i in range(len(self.colors_arr)):
            im_icon = Image.open(os.path.join(path_to_icons, files[i])).convert("RGBA")
            im_icon = im_icon.resize((self.pix_rate, self.pix_rate))

            im_icon_hr = Image.open(os.path.join(path_to_icons, files[i])).convert("RGBA")
            im_icon_hr = im_icon_hr.resize((100, 100))
            self.color_img_arr.append({self.colors_arr[i]: im_icon})
            self.color_img_arr_hr.append({self.colors_arr[i]: im_icon_hr})

    def get_not_smooth(self):
        for i in range(0, len(self.schema), 1):
            for j in range(0, len(self.schema[0]), 1):
                if self.schema[i][j].is_smooth == False:
                    return self.schema[i][j]
        return None

    def smoothing(self, smooth_step) -> None:
        # умененьшение колличества цветов
        print('Begin smoothing')
        sp = self.get_not_smooth()
        self.all_color_arr = []
        while sp != None:
            sp_arr = []
            red = 0;
            blue = 0;
            green = 0;
            # start_time = time.time()
            for i in range(len(self.schema)):
                for j in range(len(self.schema[i])):
                    if abs(self.schema[i][j].red - sp.red) <= smooth_step and \
                            abs(self.schema[i][j].green - sp.green) <= smooth_step and \
                            abs(self.schema[i][j].blue - sp.blue) <= smooth_step and \
                            self.schema[i][j].is_smooth == False:  # порядок условий важен?
                        sp_arr.append(self.schema[i][j])
                        red += self.schema[i][j].red
                        green += self.schema[i][j].green
                        blue += self.schema[i][j].blue
            red /= len(sp_arr)
            green /= len(sp_arr)
            blue /= len(sp_arr)
            # print (str(time.time() - start_time) + "for in smoothing")
            for point in sp_arr:
                point.red = round(red)
                point.green = round(green)
                point.blue = round(blue)
                point.is_smooth = True
            self.all_color_arr.append({'red': round(red),
                                       'green': round(green),
                                       'blue': round(blue)})
            sp = self.get_not_smooth()
        print('End smoothing')

    def dithering(self,name:str):
        if name == 'Floid':
            #Флойд
            for j in range(len(self.schema[0]) - 1):
                for i in range(1,len(self.schema) - 1):
                    red_error = self.old_color_schema[i][j].red - self.schema[i][j].red
                    green_error = self.old_color_schema[i][j].green - self.schema[i][j].green
                    blue_error = self.old_color_schema[i][j].blue - self.schema[i][j].blue
                    self.schema[i + 1][j].red = self.schema[i + 1][j].red + round(red_error * 7 / 16)
                    self.schema[i + 1][j].green = self.schema[i + 1][j].green + round(green_error * 7 / 16)
                    self.schema[i + 1][j].blue = self.schema[i + 1][j].blue + round(blue_error * 7 / 16)

                    self.schema[i - 1][j + 1].red = self.schema[i - 1][j + 1].red + round(red_error * 3 / 16)
                    self.schema[i - 1][j + 1].green = self.schema[i - 1][j + 1].green + round(green_error * 3 / 16)
                    self.schema[i - 1][j + 1].blue = self.schema[i - 1][j + 1].blue + round(blue_error * 3 / 16)

                    self.schema[i][j + 1].red = self.schema[i][j + 1].red + round(red_error * 5 / 16)
                    self.schema[i][j + 1].green = self.schema[i][j + 1].green + round(green_error * 5 / 16)
                    self.schema[i][j + 1].blue = self.schema[i][j + 1].blue + round(blue_error * 5 / 16)

                    self.schema[i + 1][j + 1].red = self.schema[i + 1][j + 1].red + round(red_error * 1 / 16)
                    self.schema[i + 1][j + 1].green = self.schema[i + 1][j + 1].green + round(green_error * 1 / 16)
                    self.schema[i + 1][j + 1].blue = self.schema[i + 1][j + 1].blue + round(blue_error * 1 / 16)
        elif name == 'Jarvis':
            #Джарвиса, Джудиса и Нинке
            for j in range(2, len(self.schema[0]) - 2):
                for i in range(len(self.schema) - 2):
                    red_error = self.old_color_schema[i][j].red - self.schema[i][j].red
                    green_error = self.old_color_schema[i][j].green - self.schema[i][j].green
                    blue_error = self.old_color_schema[i][j].blue - self.schema[i][j].blue
                    self.schema[i + 1][j].red = self.schema[i + 1][j].red + round(red_error * 7 / 48)
                    self.schema[i + 1][j].green = self.schema[i + 1][j].green + round(green_error * 7 / 48)
                    self.schema[i + 1][j].blue = self.schema[i + 1][j].blue + round(blue_error * 7 / 48)

                    self.schema[i - 1][j + 1].red = self.schema[i - 1][j + 1].red + round(red_error * 5 / 48)
                    self.schema[i - 1][j + 1].green = self.schema[i - 1][j + 1].green + round(green_error * 5 / 48)
                    self.schema[i - 1][j + 1].blue = self.schema[i - 1][j + 1].blue + round(blue_error * 5 / 48)

                    self.schema[i][j + 1].red = self.schema[i][j + 1].red + round(red_error * 7 / 48)
                    self.schema[i][j + 1].green = self.schema[i][j + 1].green + round(green_error * 7 / 48)
                    self.schema[i][j + 1].blue = self.schema[i][j + 1].blue + round(blue_error * 7 / 48)

                    self.schema[i + 1][j + 1].red = self.schema[i + 1][j + 1].red + round(red_error * 5 / 48)
                    self.schema[i + 1][j + 1].green = self.schema[i + 1][j + 1].green + round(green_error * 5 / 48)
                    self.schema[i + 1][j + 1].blue = self.schema[i + 1][j + 1].blue + round(blue_error * 5 / 48)

                    self.schema[i - 2][j + 1].red = self.schema[i - 2][j + 1].red + round(red_error * 3 / 48)
                    self.schema[i - 2][j + 1].green = self.schema[i - 2][j + 1].green + round(green_error * 3 / 48)
                    self.schema[i - 2][j + 1].blue = self.schema[i - 2][j + 1].blue + round(blue_error * 3 / 48)

                    self.schema[i - 2][j + 2].red = self.schema[i - 2][j + 2].red + round(red_error * 1 / 48)
                    self.schema[i - 2][j + 2].green = self.schema[i - 2][j + 2].green + round(green_error * 1 / 48)
                    self.schema[i - 2][j + 2].blue = self.schema[i - 2][j + 2].blue + round(blue_error * 1 / 48)

                    self.schema[i - 1][j + 2].red = self.schema[i - 1][j + 2].red + round(red_error * 3 / 48)
                    self.schema[i - 1][j + 2].green = self.schema[i - 1][j + 2].green + round(green_error * 3 / 48)
                    self.schema[i - 1][j + 2].blue = self.schema[i - 1][j + 2].blue + round(blue_error * 3 / 48)

                    self.schema[i][j + 2].red = self.schema[i][j + 2].red + round(red_error * 5 / 48)
                    self.schema[i][j + 2].green = self.schema[i][j + 2].green + round(green_error * 5 / 48)
                    self.schema[i][j + 2].blue = self.schema[i][j + 2].blue + round(blue_error * 5 / 48)

                    self.schema[i + 1][j + 2].red = self.schema[i + 1][j + 2].red + round(red_error * 3 / 48)
                    self.schema[i + 1][j + 2].green = self.schema[i + 1][j + 2].green + round(green_error * 3 / 48)
                    self.schema[i + 1][j + 2].blue = self.schema[i + 1][j + 2].blue + round(blue_error * 3 / 48)

                    self.schema[i + 2][j + 2].red = self.schema[i + 2][j + 2].red + round(red_error * 1 / 48)
                    self.schema[i + 2][j + 2].green = self.schema[i + 2][j + 2].green + round(green_error * 1 / 48)
                    self.schema[i + 2][j + 2].blue = self.schema[i + 2][j + 2].blue + round(blue_error * 1 / 48)

                    self.schema[i + 2][j + 1].red = self.schema[i + 2][j + 1].red + round(red_error * 3 / 48)
                    self.schema[i + 2][j + 1].green = self.schema[i + 2][j + 1].green + round(green_error * 3 / 48)
                    self.schema[i + 2][j + 1].blue = self.schema[i + 2][j + 1].blue + round(blue_error * 3 / 48)

                    self.schema[i + 2][j].red = self.schema[i + 2][j].red + round(red_error * 5 / 48)
                    self.schema[i + 2][j].green = self.schema[i + 2][j].green + round(green_error * 5 / 48)
                    self.schema[i + 2][j].blue = self.schema[i + 2][j].blue + round(blue_error * 5 / 48)
        elif name == 'Burkes':
            #Бёркеса
            for j in range(len(self.schema[0]) - 2):
                for i in range(2,len(self.schema) - 2):
                    red_error = self.old_color_schema[i][j].red - self.schema[i][j].red
                    green_error = self.old_color_schema[i][j].green - self.schema[i][j].green
                    blue_error = self.old_color_schema[i][j].blue - self.schema[i][j].blue
                    self.schema[i + 1][j].red = self.schema[i + 1][j].red + round(red_error * 8 / 32)
                    self.schema[i + 1][j].green = self.schema[i + 1][j].green + round(green_error * 8 / 32)
                    self.schema[i + 1][j].blue = self.schema[i + 1][j].blue + round(blue_error * 8 / 32)

                    self.schema[i - 1][j + 1].red = self.schema[i - 1][j + 1].red + round(red_error * 4 / 32)
                    self.schema[i - 1][j + 1].green = self.schema[i - 1][j + 1].green + round(green_error * 4 / 32)
                    self.schema[i - 1][j + 1].blue = self.schema[i - 1][j + 1].blue + round(blue_error * 4 / 32)

                    self.schema[i][j + 1].red = self.schema[i][j + 1].red + round(red_error * 8 / 32)
                    self.schema[i][j + 1].green = self.schema[i][j + 1].green + round(green_error * 8 / 32)
                    self.schema[i][j + 1].blue = self.schema[i][j + 1].blue + round(blue_error * 8 / 32)

                    self.schema[i + 1][j + 1].red = self.schema[i + 1][j + 1].red + round(red_error * 4 / 32)
                    self.schema[i + 1][j + 1].green = self.schema[i + 1][j + 1].green + round(green_error * 4 / 32)
                    self.schema[i + 1][j + 1].blue = self.schema[i + 1][j + 1].blue + round(blue_error * 4 / 32)

                    self.schema[i - 2][j + 1].red = self.schema[i - 2][j + 1].red + round(red_error * 2 / 32)
                    self.schema[i - 2][j + 1].green = self.schema[i - 2][j + 1].green + round(green_error * 2 / 32)
                    self.schema[i - 2][j + 1].blue = self.schema[i - 2][j + 1].blue + round(blue_error * 2 / 32)

                    self.schema[i + 2][j + 1].red = self.schema[i + 2][j + 1].red + round(red_error * 2 / 32)
                    self.schema[i + 2][j + 1].green = self.schema[i + 2][j + 1].green + round(green_error * 2 / 32)
                    self.schema[i + 2][j + 1].blue = self.schema[i + 2][j + 1].blue + round(blue_error * 2 / 32)

                    self.schema[i + 2][j].red = self.schema[i + 2][j].red + round(red_error * 4 / 32)
                    self.schema[i + 2][j].green = self.schema[i + 2][j].green + round(green_error * 4 / 32)
                    self.schema[i + 2][j].blue = self.schema[i + 2][j].blue + round(blue_error * 4 / 32)
        elif name == 'Sierra':
            #Сиерра
            for j in range(len(self.schema[0]) - 2):
                for i in range(2,len(self.schema) - 2):
                    red_error = self.old_color_schema[i][j].red - self.schema[i][j].red
                    green_error = self.old_color_schema[i][j].green - self.schema[i][j].green
                    blue_error = self.old_color_schema[i][j].blue - self.schema[i][j].blue
                    self.schema[i + 1][j].red = self.schema[i + 1][j].red + round(red_error * 4 / 16)
                    self.schema[i + 1][j].green = self.schema[i + 1][j].green + round(green_error * 4 / 16)
                    self.schema[i + 1][j].blue = self.schema[i + 1][j].blue + round(blue_error * 4 / 16)

                    self.schema[i - 1][j + 1].red = self.schema[i - 1][j + 1].red + round(red_error * 2 / 16)
                    self.schema[i - 1][j + 1].green = self.schema[i - 1][j + 1].green + round(green_error * 2 / 16)
                    self.schema[i - 1][j + 1].blue = self.schema[i - 1][j + 1].blue + round(blue_error * 2 / 16)

                    self.schema[i][j + 1].red = self.schema[i][j + 1].red + round(red_error * 3 / 16)
                    self.schema[i][j + 1].green = self.schema[i][j + 1].green + round(green_error * 3 / 16)
                    self.schema[i][j + 1].blue = self.schema[i][j + 1].blue + round(blue_error * 3 / 16)

                    self.schema[i + 1][j + 1].red = self.schema[i + 1][j + 1].red + round(red_error * 2 / 16)
                    self.schema[i + 1][j + 1].green = self.schema[i + 1][j + 1].green + round(green_error * 2 / 16)
                    self.schema[i + 1][j + 1].blue = self.schema[i + 1][j + 1].blue + round(blue_error * 2 / 16)

                    self.schema[i - 2][j + 1].red = self.schema[i - 2][j + 1].red + round(red_error * 1 / 16)
                    self.schema[i - 2][j + 1].green = self.schema[i - 2][j + 1].green + round(green_error * 1 / 16)
                    self.schema[i - 2][j + 1].blue = self.schema[i - 2][j + 1].blue + round(blue_error * 1 / 16)

                    self.schema[i + 2][j + 1].red = self.schema[i + 2][j + 1].red + round(red_error * 1 / 16)
                    self.schema[i + 2][j + 1].green = self.schema[i + 2][j + 1].green + round(green_error * 1 / 16)
                    self.schema[i + 2][j + 1].blue = self.schema[i + 2][j + 1].blue + round(blue_error * 1 / 16)

                    self.schema[i + 2][j].red = self.schema[i + 2][j].red + round(red_error * 3 / 16)
                    self.schema[i + 2][j].green = self.schema[i + 2][j].green + round(green_error * 3 / 16)
                    self.schema[i + 2][j].blue = self.schema[i + 2][j].blue + round(blue_error * 3 / 16)


    def draw_cage(self, img):
        image_set = ImageDraw.Draw(img)
        schema_width = len(self.schema)
        schema_heigth = len(self.schema[0])

        y_number_offset = 2
        x_number_offset = 5

        path_to_font = os.path.join(os.path.dirname(__file__), 'fonts/Roboto-Medium.ttf')
        font = ImageFont.truetype(path_to_font, 10)
        for x in range(0, schema_width, 1):
            txt = Image.new('RGBA', (self.pix_rate, self.pix_rate), color='white')
            d = ImageDraw.Draw(txt)
            d.text((0, 0), str(x), font=font, fill=000)
            rotate_img = txt.rotate(90, expand=1)
            img.paste(rotate_img, (x * self.pix_rate + (self.pix_rate * self.img_offset),
                                   (self.pix_rate * self.img_offset - self.pix_rate - y_number_offset)))
            # image_set.text((x * self.pix_rate + (self.pix_rate * 3), (self.pix_rate * 3) - self.pix_rate), str(x), (0, 0, 0), font=font)

        for y in range(0, schema_heigth, 1):
            digit_quant = len(str(y)) - 1
            image_set.text((
                           (self.pix_rate * self.img_offset) - int(self.pix_rate / 2) - (digit_quant * x_number_offset),
                           y * self.pix_rate + (self.pix_rate * self.img_offset)), str(y),
                           (0, 0, 0), font=font)

        for x in range(0, schema_width, 1):
            for y in range(0, schema_heigth, 1):
                image_set.line((
                    0, y * self.pix_rate + (self.pix_rate * self.img_offset),
                    schema_width * self.pix_rate + (self.pix_rate * self.img_offset),
                    y * self.pix_rate + (self.pix_rate * self.img_offset)), fill=128, width=1)
                image_set.line((
                    x * self.pix_rate + (self.pix_rate * self.img_offset), 0,
                    x * self.pix_rate + (self.pix_rate * self.img_offset),
                    schema_heigth * self.pix_rate + (self.pix_rate * self.img_offset)), fill=128, width=1)

        for x in range(0, schema_width, 10):
            for y in range(0, schema_heigth, 10):
                image_set.line((
                    0, y * self.pix_rate + (self.pix_rate * self.img_offset),
                    schema_width * self.pix_rate + (self.pix_rate * self.img_offset),
                    y * self.pix_rate + (self.pix_rate * self.img_offset)), fill=128, width=3)
                image_set.line((
                    x * self.pix_rate + (self.pix_rate * self.img_offset), 0,
                    x * self.pix_rate + (self.pix_rate * self.img_offset),
                    schema_heigth * self.pix_rate + (self.pix_rate * 3)), fill=128, width=3)

    def export_img_stitch(self, path_to_img: str = 'result.png') -> None:
        print('Begin export_img')
        schema_width = len(self.schema)
        schema_heigth = len(self.schema[0])
        img = Image.new('RGBA', (schema_width * self.pix_rate, schema_heigth * self.pix_rate), color='red')
        img_background = Image.new('RGBA', (schema_width * self.pix_rate, schema_heigth * self.pix_rate))
        image_set = ImageDraw.Draw(img)
        im_icon = Image.open(os.path.join(os.path.dirname(__file__), 'icon.png')).convert("RGBA")
        im_icon = im_icon.resize((self.pix_rate, self.pix_rate))
        for x in range(0, schema_width, 1):
            for y in range(0, schema_heigth, 1):
                image_set.rectangle(((x * self.pix_rate, y * self.pix_rate),
                                     ((x * self.pix_rate) + self.pix_rate, (y * self.pix_rate) + self.pix_rate)),
                                    (self.schema[x][y].red, self.schema[x][y].green, self.schema[x][y].blue))
        for x in range(0, schema_width, 1):
            for y in range(0, schema_heigth, 1):
                img_background.alpha_composite(im_icon, (x * self.pix_rate, y * self.pix_rate))
        # добавить отрисовку сетки
        img.alpha_composite(img_background, (0, 0))
        img.save(path_to_img)
        print('End export_img')

    def export_img_previwe(self, path_to_img: str = 'result_previewe.png') -> None:
        print('Begin export_img')
        pix_rate = 10  # self.pix_rate
        schema_width = len(self.schema)
        schema_heigth = len(self.schema[0])
        img = Image.new('RGBA', (schema_width * pix_rate, schema_heigth * pix_rate), color='red')
        img_background = Image.new('RGBA', (schema_width * pix_rate, schema_heigth * pix_rate))
        image_set = ImageDraw.Draw(img)
        im_icon = Image.open(os.path.join(os.path.dirname(__file__), 'icon.png')).convert("RGBA")
        im_icon = im_icon.resize((pix_rate, pix_rate))
        for x in range(0, schema_width, 1):
            for y in range(0, schema_heigth, 1):
                image_set.rectangle(
                    ((x * pix_rate, y * pix_rate), ((x * pix_rate) + pix_rate, (y * pix_rate) + pix_rate)),
                    (self.schema[x][y].red, self.schema[x][y].green, self.schema[x][y].blue))
        for x in range(0, schema_width, 1):
            for y in range(0, schema_heigth, 1):
                img_background.alpha_composite(im_icon, (x * pix_rate, y * pix_rate))
        # добавить отрисовку сетки
        img.alpha_composite(img_background, (0, 0))
        img.save(path_to_img)
        print('End export_img')

    def export_img_icon_color(self, path_to_img: str = 'result.png') -> None:
        print('Begin export_img')
        schema_width = len(self.schema)
        schema_heigth = len(self.schema[0])
        img = Image.new('RGBA', (schema_width * self.pix_rate + (self.pix_rate * self.img_offset),
                                 schema_heigth * self.pix_rate + (self.pix_rate * self.img_offset)), color='white')
        img_background = Image.new('RGBA', (schema_width * self.pix_rate + (self.pix_rate * self.img_offset),
                                            schema_heigth * self.pix_rate + (self.pix_rate * self.img_offset)))
        image_set = ImageDraw.Draw(img)

        def get_icon(color_icon: dict, x: int, y: int):
            if (self.schema[x][y].red, self.schema[x][y].green, self.schema[x][y].blue) in color_icon:
                return 1
            else:
                return 0

        for x in range(0, schema_width, 1):
            for y in range(0, schema_heigth, 1):
                image_set.rectangle(((x * self.pix_rate + (self.pix_rate * self.img_offset),
                                      y * self.pix_rate + (self.pix_rate * self.img_offset)),
                                     ((x * self.pix_rate) + self.pix_rate + (self.pix_rate * self.img_offset),
                                      (y * self.pix_rate) + self.pix_rate + (self.pix_rate * self.img_offset))),
                                    (self.schema[x][y].red, self.schema[x][y].green, self.schema[x][y].blue))
        for x in range(0, schema_width, 1):
            for y in range(0, schema_heigth, 1):
                img_background.alpha_composite(
                    list(list(filter(lambda color_icon: get_icon(color_icon, x, y), self.color_img_arr))[0].values())[
                        0], (x * self.pix_rate + (self.pix_rate * self.img_offset),
                             y * self.pix_rate + (self.pix_rate * self.img_offset)))

        self.draw_cage(img)

        img.alpha_composite(img_background, (0, 0))
        img.save(path_to_img)
        print('End export_img')

    def export_img_icon(self, path_to_img: str = 'result.png') -> None:
        print('Begin export_img')
        schema_width = len(self.schema)
        schema_heigth = len(self.schema[0])
        img_background = Image.new('RGBA', (schema_width * self.pix_rate + (self.pix_rate * self.img_offset),
                                            schema_heigth * self.pix_rate + (self.pix_rate * self.img_offset)),
                                   color='white')
        icon_width, icon_heigth = self.im.size

        def get_icon(color_icon: dict, x: int, y: int):
            if (self.schema[x][y].red, self.schema[x][y].green, self.schema[x][y].blue) in color_icon:
                return 1
            else:
                return 0

        for x in range(0, schema_width, 1):
            for y in range(0, schema_heigth, 1):
                img_background.alpha_composite(
                    list(list(filter(lambda color_icon: get_icon(color_icon, x, y), self.color_img_arr))[0].values())[
                        0], (x * self.pix_rate + (self.pix_rate * self.img_offset),
                             y * self.pix_rate + (self.pix_rate * self.img_offset)))

        self.draw_cage(img_background)

        img_background.save(path_to_img)
        print('End export_img')

    def export_img(self, path_to_img: str = '') -> None:
        self.export_img_stitch(path_to_img + 'result.png')
        self.export_img_icon(path_to_img + 'result_icons.png')
        self.export_img_icon_color(path_to_img + 'result_icons_color.png')
        self.export_img_list(path_to_img + 'result_img_list.png')

    def export_img_list(self, path_to_img: str = 'result_img_list.png'):
        def get_color_palett_number(color:tuple):
            for palette_color in self.palettes[self.choosen_palett]:
                if palette_color['color'] == color:
                    return palette_color['number']

        margin_top = 10
        margin_right = 10
        margin_left = 10
        height_width = 100
        number_of_colomn = 3
        number_of_colomn_in_colomn = 3
        row = 0
        path_to_font = os.path.join(os.path.dirname(__file__), 'fonts/Roboto-Medium.ttf')
        font = ImageFont.truetype(path_to_font, 35)
        img = Image.new('RGBA', (((height_width * number_of_colomn_in_colomn) + (margin_top + margin_right) * (number_of_colomn_in_colomn-1)) * number_of_colomn,
                                 (height_width + margin_top) * math.ceil(len(self.color_img_arr_hr) / number_of_colomn)),
                        color='white')
        image_set = ImageDraw.Draw(img)
        for i in range(0, len(self.color_img_arr_hr)):
            if self.choosen_palett == '':
                number = str(i+1)
            else:
                number = get_color_palett_number(list(self.color_img_arr_hr[i].keys())[0])
            image_set.text((margin_left + (((margin_left + height_width) * number_of_colomn_in_colomn) * (i % number_of_colomn)),
                                  margin_top + (((margin_top + height_width) * 1) * row)), number,
                (0, 0, 0), font=font)
            image_set.rectangle(((margin_left + height_width + (((margin_left + height_width) * number_of_colomn_in_colomn) * (i % number_of_colomn)),
                                  margin_top + (((margin_top + height_width) * 1) * row)), (
                                 height_width *2 + (((margin_left + height_width) * number_of_colomn_in_colomn) * (i % number_of_colomn)),
                                 height_width + ((margin_top + height_width) * 1) * row)),
                                list(self.color_img_arr_hr[i].keys())[0])
            img.alpha_composite(list(self.color_img_arr_hr[i].values())[0].resize((height_width, height_width)), (
            margin_left + height_width * 2 + (((margin_left + height_width) * number_of_colomn_in_colomn) * (i % number_of_colomn)),
            margin_top + (((margin_top + height_width) * 1) * row)))
            if (i + 1) % number_of_colomn == 0:
                row += 1
        img.save(path_to_img)

    def nulling_smoothing(self):
        for i in range(len(self.schema)):
            for j in range(len(self.schema[0])):
                self.schema[i][j].is_smooth = False

    class SchemaPoint:
        def __init__(self, r: int = 0, g: int = 0, b: int = 0, n: int = 0, ism: bool = False) -> None:
            self.red = r
            self.green = g
            self.blue = b
            self.number = n
            self.is_smooth = ism

        def copy(self, copy_object) -> None:
            self.red = copy_object.red
            self.green = copy_object.green
            self.blue = copy_object.blue
            self.number = copy_object.number
            self.is_smooth = copy_object.is_smooth

        def get_color(self, color: str) -> int:
            if color == 'red':
                return self.red
            elif color == 'green':
                return self.green
            else:
                return self.blue

        def get_red(self) -> int:
            return self.red

        def get_green(self) -> int:
            return self.green

        def get_blue(self) -> int:
            return self.blue

        def get_number(self) -> int:
            return self.number

        def get_smooth(self) -> int:
            return self.is_smooth

        def set_red(self, red: int) -> None:
            self.red = red

        def set_green(self, green: int) -> None:
            self.green = green

        def set_blue(self, blue: int) -> None:
            self.blue = blue

        def set_number(self, number: int) -> None:
            self.red = number

        def set_is_smooth(self, is_smooth: bool) -> None:
            self.is_smooth = is_smooth


css = CrossStitchSchema()
