# -*- coding: utf-8 -*-
import socket, ssl, time, sqlite3, os, re, threading, requests, traceback, datetime
from db_saver import Db_writer

def log_chat(channel):
    ircsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server = "irc.chat.twitch.tv" # Server
    botnick = "xxxxxxxx" # Your bots nick

    ircsock.connect((server, 6667))

    ircsock.send(bytes("PASS oauth:xxxxxxxxxxxxxxxxxxxxx " + "\r\n", "UTF-8"))
    ircsock.send(bytes("NICK " + botnick + "\r\n", "UTF-8"))

    ircmsg = ircsock.recv(10000).decode("UTF-8")
    #print(ircmsg)

    ircsock.send(bytes("JOIN #" + channel + "\r\n", "UTF-8"))

    time.sleep(1)

    ircmsg = ircsock.recv(10000).decode("UTF-8", "ignore")
    #print(ircmsg)

    #ircsock.send(bytes("PRIVMSG #" + channel + " :Kappa" + "\r\n", "UTF-8"))

    time.sleep(1)
    #print("Лоигрование для канала " + channel + " запущено \n")
    begin_time = time.time()
    message = ""
    while True:
        try:
            if time.time() - begin_time >= 21600:
                print("Past 6 hours,stop channel " + channel)
                return
            ircmsg = ircsock.recv(1000000).decode("UTF-8")
            msg_arr = ircmsg.split("\n")
            #Последний элемент - символ перевода строки \n
            for i in range(len(msg_arr)-1):
                if msg_arr[i][:-1] == "PING :tmi.twitch.tv":
                    ircsock.send(bytes("PONG :tmi.twitch.tv" + "\r\n", "UTF-8"))
                elif msg_arr[i].find('twitch.tv 366') != -1 or \
                     msg_arr[i].find('twitch.tv 353') != -1:
                    pass #ничего не делать, если встретили такое сообщение
                else:
                    #print(msg_arr[i])
                    message = msg_arr[i]
                    msg_nick = msg_arr[i].split("PRIVMSG")
                    nick = msg_nick[0].split("!")[0].split(":")[1]
                    msg = re.sub("'",'\'\'',msg_nick[1][4 + len(channel):])
                    if nick != "moobot" and \
                        nick != "nightbot" and \
                        nick != "liz0bot" and \
                        nick != "streamelements" and \
                        nick != "etozhebot" and \
                        nick != "wlgboy":
                        #print( "?" + channel + "?" + time.ctime(time.time()) + ": "+ nick + ": " + msg)
                        Db_writer.queue_chat.append({'nick' :nick
                                                    ,'channel':channel
                                                    ,'msg':msg
                                                    ,'timestamp':datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')})
        except BaseException:
            print("Error for channel " + channel)
            print("Message: " + message)
            raise Exception("Restart thread exceprion")
            
def wrapped_log_chat(channel):
    while True:
        try:
            print("Loging start for " + channel + "\n")
            log_chat(channel)
        except:
            traceback.print_exc()

def log_viewers(channels):
    bearer_token = get_auth_token()
    headers = {'Client-ID': 'xxxxxxxxxxxxxxxxxxxx',
               'Authorization' : 'Bearer ' + bearer_token}
    url = "https://api.twitch.tv/helix/streams?"
    for i in range(len(channels)):
        if i == 0:
            url += "user_login=" + channels[i]
        else:
            url += "&user_login=" + channels[i]
    while True:
        try:
            response = requests.get(url,headers = headers)
            if response.json().get("error") == 'Unauthorized':
                bearer_token = get_auth_token()
                headers = {'Client-ID': 'xxxxxxxxxxxxxxxxxxx',
                           'Authorization' : 'Bearer ' + bearer_token}
            if response.json().get("data") != None:
                for i in range(len(response.json().get("data"))):
                    Db_writer.queue_viewers.append({"channel": response.json()["data"][i]['user_name'].lower()
                                                    ,"quantity" : response.json()["data"][i]['viewer_count']
                                                    ,'timestamp':datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')})
            time.sleep(60)
        except:
            traceback.print_exc()

def get_auth_token():
    url = "https://id.twitch.tv/oauth2/token?client_id=xxxxxxxxxx"
    response = requests.post(url)
    return response.json().get("access_token")

streams = ['list_of_steams']
headers = {'Client-ID': 'xxxxxxxxxxxxxxxxx',
           'Accept': 'application/vnd.twitchtv.v5+json'}

#response = requests.get('https://api.twitch.tv/kraken/streams/?language=ru&limit=100',headers = headers)

#for i in range(len(response.json()["streams"])):
    #streams.append(response.json()["streams"][i]["channel"]["name"])

streams = list(set(streams))

thread_arr = []

for i in range(len(streams)):
    thread_arr.append(threading.Thread(target=wrapped_log_chat, args=(streams[i],)))

for i in range(len(thread_arr)):
    thread_arr[i].daemon = False
    thread_arr[i].start()

thread_viewers = threading.Thread(target=log_viewers, args=(streams,))
thread_viewers.daemon = False
thread_viewers.start()

thread_db = threading.Thread(target=Db_writer.write_db)
thread_db.daemon = False
thread_db.start()
print("Start - " + str(time.ctime(time.time())))
    


