import socket, ssl, time, os, re, threading, requests, traceback
import pymysql

class Db_writer:
    queue_chat = []
    queue_viewers = []
    begin_db_write = time.time()
    number_of_message = 0
            
    @staticmethod
    def write_queue_chat(conn):
        cursor = conn.cursor()
        if time.time() - Db_writer.begin_db_write >= 600:
            print(time.ctime(time.time()) + ": " + str(Db_writer.number_of_message))
            Db_writer.begin_db_write = time.time()
            Db_writer.number_of_message = 0
        last_index = len(Db_writer.queue_chat)
        nicks = []
        channels = []
        all_inf =[]
        #print(len(Db_writer.queue_chat))
        for i in range(last_index):
            nicks.append((Db_writer.queue_chat[i]['nick'],Db_writer.queue_chat[i]['nick'],))
            channels.append((Db_writer.queue_chat[i]['channel'],Db_writer.queue_chat[i]['channel'],))
            all_inf.append((Db_writer.queue_chat[i]['msg'],Db_writer.queue_chat[i]['nick'],Db_writer.queue_chat[i]['channel'],Db_writer.queue_chat[i]['timestamp'],))
        sql = """
                            INSERT INTO users(nick)
                            SELECT %s
                            WHERE NOT EXISTS (SELECT 1
                                                FROM users
                                                WHERE nick = %s)
                            """
        cursor.executemany(sql,nicks)

        sql = """
                            INSERT INTO channels(name)
                            SELECT %s
                            WHERE NOT EXISTS (SELECT 1
                                                FROM channels
                                                WHERE name = %s)
                            """
        cursor.executemany(sql,channels)
                                
        sql = """
                            INSERT INTO messages(msg,user_id,id_channel,timestamp)
                            VALUES(%s
                                    ,(SELECT id
                                        FROM users
                                        WHERE nick = %s)
                                    ,(SELECT id
                                        FROM channels
                                        WHERE name = %s)
                                    ,%s)
                            """
        cursor.executemany(sql,all_inf)
        cursor.close()
        conn.commit()
        del Db_writer.queue_chat[0:last_index]
        Db_writer.number_of_message += last_index
        
    @staticmethod
    def write_queue_viewers(conn):
        cursor = conn.cursor()
        last_index = len(Db_writer.queue_viewers)
        channels_viewers = []
        for i in range(last_index):
            channels_viewers.append((Db_writer.queue_viewers[i]["channel"],Db_writer.queue_viewers[i]["quantity"],Db_writer.queue_viewers[i]["timestamp"]))
        sql = """
                INSERT INTO viewers(id_channel,quantity,timestamp)
                VALUES ((SELECT c.id FROM channels c WHERE c.name = %s)
                       ,%s
                       ,%s)
                 """
        cursor.executemany(sql,channels_viewers)
        conn.commit()
        del Db_writer.queue_viewers[0:last_index]
        
    @staticmethod
    def write_db():
        conn = pymysql.connect(
            host='192.168.0.101',
            user='admin',
            password='admin',
            db='chat_db',
            charset='utf8mb4'
        )
        cursor = conn.cursor()
        cursor.execute("""
                        SELECT COUNT(*)
                        FROM information_schema.tables
                        WHERE table_name = '{0}'
                          AND table_schema = 'chat_db'
                        """.format('users'))
        if cursor.fetchone()[0] == 0:
            cursor.execute("""CREATE TABLE users
                          (id INTEGER AUTO_INCREMENT
                          ,nick text
                          ,CONSTRAINT users_pk PRIMARY KEY (id)
                          )
                           """)
            cursor.execute("""CREATE TABLE messages
                          (id INTEGER AUTO_INCREMENT
                          ,msg text
                          ,user_id INTEGER
                          ,id_channel INTEGER
                          ,timestamp DATETIME DEFAULT CURRENT_TIMESTAMP
                          ,CONSTRAINT messages_pk PRIMARY KEY (id)
                          )
                           """)
            cursor.execute("""CREATE TABLE channels
                          (id INTEGER AUTO_INCREMENT
                          ,name text
                          ,timestamp DATETIME DEFAULT CURRENT_TIMESTAMP
                          ,CONSTRAINT channels_pk PRIMARY KEY (id)
                          )
                           """)
            cursor.execute("""CREATE TABLE viewers
                              (
                                id INTEGER AUTO_INCREMENT
                               ,id_channel INTEGER
                               ,quantity 	INTEGER
                               ,timestamp datetime DEFAULT CURRENT_TIMESTAMP
                               ,CONSTRAINT viewers_pk PRIMARY KEY (id)
                              )
                           """)
            conn.commit()
        while True:
            try:
                Db_writer.write_queue_chat(conn)
                Db_writer.write_queue_viewers(conn)
                time.sleep(1)
            except:
                traceback.print_exc()
                while conn.open != True:
                    try:
                        conn = pymysql.connect(
                            host='192.168.0.101',
                            user='admin',
                            password='admin',
                            db='chat_db',
                            charset='utf8mb4'
                        )
                    except:
                        pass
        conn.close()
