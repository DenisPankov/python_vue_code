from flask import Flask, current_app,jsonify, request
import pymysql

app = Flask(__name__)

@app.route('/')
def index() -> str:
    return current_app.send_static_file('html/index.html')

@app.route('/favicon.ico')
def get_fivecon() -> str:
    return current_app.send_static_file('img/favicon.ico')

@app.route('/api/user_message/<user>')
def get_all_user_message(user) -> str:
    conn = pymysql.connect(
            host='192.168.0.101',
            user='admin',
            password='admin',
            db='chat_db',
            charset='utf8mb4'
        )
    cursor = conn.cursor()
    cursor.execute("""
                    SELECT m.msg
                      FROM messages m
                     where m.user_id = (SELECT u.id
                                          FROM users u
                                         WHERE u.nick = '{0}')
                    """.format(user))
    results = cursor.fetchall()
    messages = []
    for result in results:
        messages.append(result[0])
    return jsonify(messages)

@app.route('/api/main_inf', methods=['POST','GET'])
def get_main_inf() -> str:
    conn = pymysql.connect(
            host='192.168.0.101',
            user='admin',
            password='admin',
            db='chat_db',
            charset='utf8mb4'
        )
    cursor = conn.cursor()
    cursor.execute("""
                    SELECT name
                          ,value
                      FROM key_value_storage kvs
                     where kvs.name in ('message_quantity'
                                       ,'users_quantity'
                                       ,'channels_quantity')
                    """)
    results = cursor.fetchall()
    response = {results[0][0] : results[0][1],
                results[1][0] : results[1][1],
                results[2][0] : results[2][1]}
    return jsonify(response)

@app.route('/api/channel/unlive_messages', methods=['POST'])
def get_channel_unlive_messages() -> str:
    channel_name = request.values['channel_name']
    date = request.values['date']
    conn = pymysql.connect(
            host='192.168.0.101',
            user='admin',
            password='admin',
            db='chat_db',
            charset='utf8mb4'
        )
    cursor = conn.cursor()
    sql = """
                    SELECT u.nick
                          ,m.msg
                          ,m.`timestamp`
                          ,m.id 
                    from messages m
                    join users u on u.id = m.user_id
                    where (DATE_FORMAT(m.timestamp, '%d.%m.%Y %H:%i:00') not in (SELECT DATE_FORMAT(v.timestamp, '%d.%m.%Y %H:%i:00')
						                                                   from viewers v
						                                                  where v.id_channel = (SELECT c.id
						                                                                        FROM channels c
						                                                                       where c.name = '{0}'))
						  AND 
						  ((DATE_FORMAT(DATE_ADD(m.timestamp, INTERVAL 1 MINUTE), '%d.%m.%Y %H:%i:00') not in (SELECT DATE_FORMAT(v.timestamp, '%d.%m.%Y %H:%i:00')
						                                                   from viewers v
						                                                  where v.id_channel = (SELECT c.id
						                                                                        FROM channels c
						                                                                       where c.name = '{0}')))
						   OR
						   DATE_FORMAT(DATE_ADD(m.timestamp, INTERVAL -1 MINUTE), '%d.%m.%Y %H:%i:00') not in (SELECT DATE_FORMAT(v.timestamp, '%d.%m.%Y %H:%i:00')
						                                                   from viewers v
						                                                  where v.id_channel = (SELECT c.id
						                                                                        FROM channels c
						                                                                       where c.name = '{0}')))
						  )
                    and m.id_channel = (SELECT c.id
                                            FROM channels c
                                           where c.name = '{0}')
                    and DATE_FORMAT(m.timestamp, '%d.%m.%Y') = '{1}'
                    order by m.`timestamp` desc
                    """.format(channel_name,date)
    cursor.execute(sql)
    results = cursor.fetchall()
    messages = []
    for result in results:
        messages.append(result[0])
    return jsonify(results)
app.run(debug=True)
