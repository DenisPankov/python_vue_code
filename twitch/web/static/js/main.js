site = '127.0.0.0:5000'

Vue.component('main_inf', {
	data: function(){
		return {
			msg_quant: null,
			usr_quant: null,
			channel_quant: null,
			msg_delta: null,
			usr_delta: null
		}
	},
	methods: {
		get_data: function(){
			component = this;
			$.ajax({
					type: 'POST',
					url: 'api/main_inf',
					success: function(data){
						component.msg_quant = parseInt(data.message_quantity).toLocaleString();
						component.usr_quant = parseInt(data.users_quantity).toLocaleString();
						component.channel_quant = parseInt(data.channels_quantity).toLocaleString();
					},
				});
			setInterval(function(){
				$.ajax({
					type: 'POST',
					url: 'api/main_inf',
					success: function(data){
						component.msg_delta = ' + ' + (parseInt(data.message_quantity) - parseInt(component.msg_quant.split(' ').join('')));
						component.usr_delta = ' + ' + (parseInt(data.users_quantity) - parseInt(component.usr_quant.split(' ').join('')));
						
						component.msg_quant = parseInt(data.message_quantity).toLocaleString();
						component.usr_quant = parseInt(data.users_quantity).toLocaleString();
						component.channel_quant = parseInt(data.channels_quantity).toLocaleString();
					},
				});
			},60000)
		},
	},
	mounted: function(){
		this.get_data()
	},
	template: 	`<div id="main_inf">
				Сообщений: {{ msg_quant }}  <div class="delta_main_info">{{ msg_delta }}</div> &nbsp;
				Пользователей: {{ usr_quant }}	 <div class="delta_main_info">{{ usr_delta }}</div> &nbsp;
				Каналов: {{ channel_quant }}
				</div>`,
});

Vue.component('search', {
	data: function(){
		return {
			request: null
		}
	},
	methods: {
		get_data: function(){
			component = this;
			$.ajax({
					type: 'POST',
					url: 'api/main_inf',
					success: function(data){
					},
				});
		},
	},
	mounted: function(){
	},
	template: 	`<div id="search">
					<input type="text">
				</div>`,
});

$( document ).ready(function() {
	var app = new Vue({
	  el: '#app'
	})
});
