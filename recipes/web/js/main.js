var site = " https://localhost";

Vue.use(VueRouter);

var mainPage = Vue.component('main-page', {
	template: `	<div id='main-page'>
				<selectingredients></selectingredients>
					<div id = 'recipe_view'>
						<main-view></main-view>
					</div>
				</div>`,
});

Vue.component('main-view',{
	data: function(){
		return {
			title: 'Как это работает?',
			text: `<p>В поле ингредиент начните вводить название.</p>
				   <p>Например - картофель.</p>
				   <p>Выберите ингредиент. Ниже появятся доступные рецепты</p>`,
			ingredients: null,
			img: null,
		}
	},
	methods:{
		getRecipe: function(id){ // получить данные о рецепте
			/*
			*/
			var component = this;
			if(id == null || id == undefined){
				if (this.$route.params.id == null || this.$route.params.id == undefined)
					return;
				id = this.$route.params.id; 
			}
			this.$router.push('/' + id);
			component.title = null;
			component.text = null;
			component.ingredients = null;
			component.img = null;
			$.ajax({
				type: 'GET',
				url: 'recipes/search-by-id?id=' + id,
				success: function(data){
					component.title = data.title;
					component.text = data.text;
					component.ingredients = data.ingredients;
					component.img = data.main_pic;
					$([document.documentElement, document.body]).animate({
						scrollTop: $("#recipe_view").offset().top
					}, 500);
				},
				error: function(data){
					console.log(data);
				},
			});
		},
	},
	mounted: function(){ // хук. в нем идет обработка события из другого компонента
		component = this;
		this.$root.$on('show_recipe', function(id){ // вещаю на событие метод показа рецепта
			component.getRecipe(id);
		});
		this.getRecipe(); // вызываю метод при переходе на роут показа рецепта
	},
	template: `<div class="recipe_wrapper">
				   <progress_bar v-show="title == null"></progress_bar>
				   <h1 class="title">{{title}}</h1>
				   <div class="img_wrapper">
						<img v-bind:src="img"/>
				   </div>
				   <div class="ingredients" v-show="ingredients!=null">
						<ul>
							<li v-for="ingredient in ingredients">
							<div class="name">{{ingredient.name}}</div>
							<div class="line"></div>
							<div class="dosage">{{ingredient.dosage}}</div>
							</li>
						</ul>
				   </div>
				   <div class="body" v-html="text"></div>
			   </div>`
});

Vue.component('selectingredients', {
	data: function(){
		return {
			ingredients_search: null,
			ingredients_select: null,
			found_recipes: null,
			drop_flag_busy:false, //нужен, чтобы сделать только один запрос для дропдауна
			progress_flag:false, // показывать ли прогремм бар
		}
	},
	methods: {
		dropdown: function(data){ //получает списко коспонентов для дропдауна
			var inputValue = data.target.value.toLowerCase();
			var component = this;
			if (inputValue.length > 2 && this.drop_flag_busy == false){
				$.ajax({
					type: 'GET',
					url: 'ingredients/search?name='+inputValue,
					context: component,
					success: function(data){
						if (data.length > 0){
							component.ingredients_search = data;
						}
						this.drop_flag_busy = false;
					}
				});
				this.drop_flag_busy = true;
			}else{
				component.ingredients_search = null;
			}
		},
		select_ingredient: function(event){ // вызывается при выбирании компонента из дропдауна
			if(this.ingredients_select == null){
				this.ingredients_select = [event.target.innerText];
			}else{
				if (this.ingredients_select.indexOf(event.target.innerText)== -1){
					this.ingredients_select.push(event.target.innerText);
				}
			}
			this.ingredients_search = null;
			$('input').val("");
			this.search_by_ingredient();
		},
		search_by_ingredient: function(){// производит поиск по ингредиентам
			var component = this;
			component.found_recipes = null;
			component.progress_flag = true;
			ingr_in_str = "";
			for(var i = 0; i < component.ingredients_select.length ; i++){
				ingr_in_str += "ingr" + i + "="+ component.ingredients_select[i]+"&";
			}
			ingr_in_str = ingr_in_str.slice(0, ingr_in_str.length-1);
			$.ajax({
					type: 'GET',
					url: 'recipes/search?' + ingr_in_str,
					context: component,
					success: function(data){
						component.found_recipes = data;
						component.found_recipes.sort(component.compare_recipe_by_ingredients);
						component.progress_flag = false;
					}
				});
		},
		delete_selected: function(){ // удаление выбранного ингредиента
			this.ingredients_select.splice(this.ingredients_select.indexOf(event.target.innerText),1);
			this.search_by_ingredient();
		},
		show_recipe: function(data){ // функция связанная с другим компонентом. генерирует событие, на которое подписан компонент выводящий рецепты
			var id = null;
			for (var i = 0; i < this.found_recipes.length ; i++){
				if(this.found_recipes[i].title == data.target.innerText)
					id = this.found_recipes[i].id;
			}
			this.$root.$emit('show_recipe', id);
			$([document.documentElement, document.body]).animate({
				scrollTop: $("#recipe_view").offset().top
			}, 500);
		},
		compare_recipe_by_ingredients(rec1, rec2){
			return (rec1.ingredients.length - rec2.ingredients.length) * -1;
		},
	},
	updated: function(){
		$( document ).ready(function() {
			if (window.matchMedia("(min-width: 1300px)").matches){
				var img = $(".list_img");
				for(var i = 0 ; i < img.length ; i++){
					$(img[i]).blowup({
						"round": false,
						"background" : "#FFF",
						"width" : 250,
						"height" : 250,
						"scale": 0.4,
					});
				}
			}
		});
	},
	template: `	<div id = 'choose_wrapper'>
					<div id = 'choose_components'>
						<div class="select_wripper">
						   <h1 id="search_title">Поиск</h1>
						   <div id="input_wrapper">
								<div id="wrapper_for_dropdown">
									<input type = "text" v-on:input = "dropdown" placeholder = "ингредиент">
									<div class = "dropdown" v-show="ingredients_search!=null">
										<div class="ingredient" v-for= "ingredient in ingredients_search" @click="select_ingredient">{{ingredient.name}}</div>
								   </div>
							   </div>
						   </div>
						   <div class="selected_ingredients">
								<div class="ingredient" v-for= "ingredient in ingredients_select" @click="delete_selected">{{ingredient}}</div>
						   </div>
						   </div>
					</div>
					<h1 id="choose_title">Найденные рецепты</h1>
					<div id = 'choose_recipe'>
						<progress_bar v-show="found_recipes == null && progress_flag == true"></progress_bar>
						<div class="found_recipes" v-for= "recipe in found_recipes" @click = "show_recipe">
							<div class="wrapper_pseudo_table">
								<div class="title">{{recipe.title}}</div>
							</div>
							<img class="list_img" :src="recipe.main_pic"/>
						</div>
					</div>
				</div>`
});

var allRecipes = Vue.component('all-recipes', {
	data: function(){
		return {
			recipes: null,
			prev_page: null,
			next_page: null,
			site_url: site,
		}
	},
	methods: {
		getAllRecipes: function(){
			var component = this;
			page = this.$route.params.page;
			component.recipes = null;
			$.ajax({
				type: 'GET',
				url: site+'/recipes?page=' + page,
				context: component,
				success: function(data, textStatus, jqXHR){
					component.recipes = data;
					links = jqXHR.getResponseHeader('link').split(',');
					for (var i = 0; i< links.length ; i++){
						links[i] = links[i].split(';')
					}
					for (var i = 0; i< links.length ; i++){
						if(links[i][1].indexOf('rel=self')==1){
							component.prev_page = links[i][0].slice(-2, -1);
							component.next_page = links[i][0].slice(-2, -1);							
						}
						if(links[i][1].indexOf('rel=prev')==1){
							component.prev_page = links[i][0].slice(-2, -1);				
						}
						if(links[i][1].indexOf('rel=next')==1){
							component.next_page = links[i][0].slice(-2, -1);							
						}
					}
				}
			});
		},
	},
	watch:{
		 '$route': function(to, from) {
			this.getAllRecipes();
		}
	},
	mounted: function(){
		this.getAllRecipes();
	},
	updated: function(){
		$( document ).ready(function() {
			if (window.matchMedia("(min-width: 1300px)").matches){
				var img = $("img");
				for(var i = 0 ; i < img.length ; i++){
					$(img[i]).blowup({
						"round": false,
						"background" : "#FFF",
						"width" : 250,
						"height" : 250,
						"scale": 0.4,
					});
				}
			}
		});
	},
	template:`	<div id="all_rec_wripper">
					<div class="recipes_list">
							<progress_bar v-show="recipes == null"></progress_bar>
							<div class="recipe" v-for="recipe in recipes">
							<router-link :to="{path:'/'+recipe.id}">
									<div class="title">{{recipe.title}}</div>
									<img :src="site_url+'/'+recipe.main_pic"/>
							</router-link>
							</div>
					</div>
					<div class="pagination">
						<router-link class="no_a_decor" :to="'/page/allrecipes/' + prev_page">
						<div class="prev_btn btn">Prev</div>
						</router-link>
						<router-link class="no_a_decor" :to="'/page/allrecipes/' + next_page">
						<div class="next_btn btn">Next</div>
						</router-link>
					</div>
				</div>`,
});

var login = Vue.component('login', {
	data: function(){
		return {
			message: null,
			username: null,
			pass: null,
		}
	},
	methods: {
		check_data: function(){
			component = this;
			$.ajax({
					type: 'POST',
					url: site + '/user/login',
					context: component,
					data: {username: component.username , pass: component.pass},
					success: function(data){
						if(data.length == 0){
							component.message = "Неправильная пара username, pass";
						}
						else{
							localStorage.setItem('token', data[0]['token']);
							this.$router.push('/page/admin');
						}
					}
				});
		},
	},
	mounted: function(){
		component = this;
		if(localStorage.getItem('token') == null){
			localStorage.setItem('token', '1');
		}
		$.ajax({
				type: 'POST',
				url: site + '/user/check',
				context: component,
				data: {token: localStorage.getItem('token')},
				success: function(data){
					if (data == '200'){
						this.$router.push('/page/admin');
					}	
				}
			});
	},
	template: 	`<div class="login_wrapper">
					<div id="login_form">
						<input v-model="username" type="text" placeholder="username">
						<input v-model="pass" type="password" placeholder="pass">
						<div class="btn btn_sbt" @click="check_data">Войти</div>
						<div id="message">{{message}}</div>
					</div>
				</div>`,
});

var admin = Vue.component('admin', {
	data: function(){
		return {
			current_body: 1,
		}
	},
	methods: {
		exit: function(){
			localStorage.removeItem('token');
			this.$router.push('/page/login');
		},
	},
	mounted: function(){
		// такой способ проверки пользователя не безопасен
		// проверка должна происходить на сервере и сервер должен возвращать страницу или переадресовывать на страницу логина
		component = this;
		if (localStorage.getItem('token') == null){
			this.$router.push('/page/login');
		}else{
			$.ajax({
					type: 'POST',
					url: site + '/user/check', 
					context: component,
					data: {token: localStorage.getItem('token')},
					success: function(data){
						if (data == '403'){
							this.$router.push('/page/login');
						}
					}
				});
		}
	},
	template: 	`<div class="admin_wrapper">
					<div id="menu">
						<div class="menu_point" v-on:click="current_body=1">Добавить рецепт</div>
						<div class="menu_point" v-on:click="current_body=2">Изменить рецепт</div>
						<div class="menu_point" v-on:click="current_body=3">Удалить рецепт</div>
						<div class="menu_point" v-on:click="current_body=4">Управлять компонентами</div>
						<div class="menu_point" @click="exit">Выйти</div>
					</div>
					<div id="body">
						<add_recipe v-if="current_body==1"></add_recipe>
						<change_recipe v-if="current_body==2"></change_recipe>
						<delete_recipe v-if="current_body==3"></delete_recipe>
						<manage_ingredients v-if="current_body==4"></manage_ingredients>
					</div>
				</div>`,
});

Vue.component('add_recipe', {
	data: function(){
		return {
			ingredients: [""], 
			dosages: [""],
			ingredients_search: null,
			current_dropdown: null,
			message: null,
			pic_path: 'pic/default.jpg',
			site: site,
			drop_flag_busy:false, //нужен, чтобы сделать только один запрос для дропдауна
		}
	},
	methods: {
		dropdown: function(data, id){ //получает списко коспонентов для дропдауна
			var inputValue = data;
			var component = this;
			if (inputValue.length > 2 && this.drop_flag_busy==false){
				$.ajax({
					type: 'GET',
					url: site + '/ingredients/search?name='+inputValue,
					context: component,
					success: function(data){
						if (data.length > 0){
							component.ingredients_search = data;
							this.current_dropdown = id;
						}
						this.drop_flag_busy = false;
					}
				});
				this.drop_flag_busy = true;
			}else{
				component.ingredients_search = null;
			}
		},
		select_ingr: function(data){
			this.ingredients[this.current_dropdown] = data.target.innerText;
			$(data.target).parent().parent().children('input').val(data.target.innerText);
			this.ingredients_search = null;
		},
		change_num_of_ingr: function(event){
			var flag = false;
			if(event.target.id.slice(0, -1) == "ingr"){
				this.dropdown(event.target.value, event.target.id.slice(-1));
			}else if(event.target.id.slice(0, -1) == "dosage"){
				this.dosages[event.target.id.slice(-1)] = event.target.value;
			}
			for (var i = 0; i < this.ingredients.length ; i++){
				if(this.ingredients[i] == "" && this.dosages[i] == ""){
					this.ingredients.splice(i, 1);
					this.dosages.splice(i, 1);
				}
				if(this.ingredients[i] == "" || this.dosages[i] == ""){
					flag = true;
				}
			}
			if (flag == true)
				return
			this.ingredients.push("");
			this.dosages.push("");
		},
		add_recipe: function(){
			for(var i = 0 ; i < this.ingredients.length ; i++){
				if(this.ingredients[i] == "" || this.dosages[i] == ""){
					this.ingredients.splice(i, 1);
					this.dosages.splice(i, 1);
				}
			}
			var recipe = {title: $('#title').val(), text: $('.ql-editor').html(), main_pic: this.pic_path}
			for(var i = 0 ; i < this.ingredients.length ; i++){
				recipe['ingredient' + i] = this.ingredients[i];
				recipe['dosage' + i] = this.dosages[i];
			}
			var component = this;
			$.ajax({
				type: 'POST',
				url: site + '/recipes',
				context: component,
				headers:{
					Authorization: "Bearer " + localStorage.getItem('token'),
				},
				data: recipe,
				success: function(data){
					this.message = 'Рецепт успешно добавлен'
				},
				error: function(data){
					this.message = 'Произошла ошибка'
				},
			});
		},
	},
	mounted: function(){
		component1 = this;
		var toolbarOptions = ['bold', 'link', 'italic', 'underline', 'strike', 'video', 'image', 'align','blockquote'];
		var quill = new Quill('#editor', {
			modules: {
			  toolbar: '#toolbar',
			},
			theme: 'snow',
		});
		this.$root.$on('get_pic_path', function(path){
			component1.pic_path = path;
		});
		
		this.$root.$on('upload_success', function(){
			component1.$root.$emit('update_img');
		});
	},
	template: 	`<div class="add_change">
					<input type="text" id="title" placeholder="Заголовок">
					<div id="ingredients">
						<div id = "wrap_ingr">
							<div class="ingredient" v-for="(ingredient, index) in ingredients">
								<input v-bind:id="'ingr' + index" type="text" class="name" v-on:input="change_num_of_ingr" placeholder="название ингредиента">
								<div class = "dropdown" v-show="ingredients_search!=null && current_dropdown == index">
									<div class="ingredient" v-for= "ingredient in ingredients_search" @click ="select_ingr" >{{ingredient.name}}</div>
								</div>
							</div> 
						</div>
						<div id = "wrap_dosage">
						<div class="dosage" v-for="(dosage, index) in dosages">
							<input v-bind:id="'dosage' + index" type="text" class="dosage" v-on:input="change_num_of_ingr" placeholder="количество">
						</div> 
						</div>
					</div>
					<img id="main_img" :src="site +'/'+pic_path">
					<upload_img></upload_img>
					<img_picker></img_picker>
					<div id="toolbar">
					  <select class="ql-size">
						<option value="10px">Small</option>
						<option value="13px" selected>Normal</option>
						<option value="18px">Large</option>
						<option value="32px">Huge</option>
					  </select>
					  <span class="ql-formats">
						<button class="ql-align" value=""></button>
						<button class="ql-align" value="center"></button>
						<button class="ql-align" value="right"></button>
						<button class="ql-align" value="justify"></button>
					  </span>
					  <button class="ql-bold"></button>
					  <button class="ql-italic"></button>
					  <button class="ql-strike"></button>
					  <button class="ql-underline"></button>
					  <button class="ql-link"></button>
					  <button class="ql-image"></button>
					</div>
					<div id="editor_wrapper">
						<div id="toolbar"></div>
						<div id="editor"></div>
					</div>
					<div class="btn_add" @click = "add_recipe">Добавить</div>
					{{message}}
				</div>`,
});

Vue.component('change_recipe', {
	data: function(){
		return {
			ingredients: [], 
			dosages: [],
			ingredients_search: null,
			current_dropdown: null,
			title:null,
			text: null,
			message: null,
			id: null,
			quill: null,
			pic_path: null,
			site: site,
			drop_flag_busy:false, //нужен, чтобы сделать только один запрос для дропдауна
		}
	},
	methods: {
		dropdown: function(data, id){ //получает списко коспонентов для дропдауна
			var inputValue = data;
			var component = this;
			if (inputValue.length > 2 && this.drop_flag_busy==false){
				$.ajax({
					type: 'GET',
					url: site + '/ingredients/search?name='+inputValue,
					context: component,
					success: function(data){
						if (data.length > 0){
							this.ingredients_search = data;
							this.current_dropdown = id;
						}
						this.drop_flag_busy = false;
					},
					error: function(data){
						console.log(data);
					},
				});
				this.drop_flag_busy = true;
			}else{
				component.ingredients_search = null;
			}
		},
		select_ingr: function(data){
			this.ingredients[this.current_dropdown] = data.target.innerText;
			$(data.target).parent().parent().children('input').val(data.target.innerText);
			this.ingredients_search = null;
		},
		change_num_of_ingr: function(event){
			var flag = false;
			if(event.target.id[0] == "i"){
				this.ingredients[event.target.id.replace('ingr',"")] = event.target.value;
				this.dropdown(event.target.value, event.target.id.replace('ingr',""))
			}else if(event.target.id[0] == "d"){
				this.dosages[event.target.id.replace('dosage',"")] = event.target.value;
			}
			for (var i = 0; i < this.ingredients.length ; i++){
				if(this.ingredients[i] == "" && this.dosages[i] == ""){
					this.ingredients.splice(i, 1);
					this.dosages.splice(i, 1);
				}
				if(this.ingredients[i] == "" || this.dosages[i] == ""){
					flag = true;
				}
			}
			if (flag == true)
				return
			this.ingredients.push("");
			this.dosages.push("");
		},
		change_recipe: function(){
			component = this;
			for(var i = 0 ; i < this.ingredients.length ; i++){
				if(this.ingredients[i] == "" || this.dosages[i] == ""){
					this.ingredients.splice(i, 1);
					this.dosages.splice(i, 1);
				}
			}
			this.text = this.quill.root.innerHTML;
			var recipe = {id: this.id, title: this.title, text: this.text, ingredients: this.ingredients, dosage: this.dosages, main_pic: this.pic_path};
			$.ajax({
				type: 'PUT',
				url: site + '/recipes/'+this.id,
				context: component,
				headers:{
					Authorization: "Bearer " + localStorage.getItem('token'),
				},
				data: recipe,
				success: function(data){
					this.search_recipe();
					this.message = "Изменение проведено успешно"
				},
				error: function(data){
					console.log(data);
				},
			});
		},
		search_recipe: function(){
			component = this;
			$.ajax({
				type: 'GET',
				url: site + '/recipes/search-by-id?id=' + this.id,
				context: component,
				success: function(data){
					this.title = data.title;
					this.text = data.text;
					this.ingredients = [];
					this.dosages = [];
					for(var i = 0; i< data.ingredients.length; i++){
						this.ingredients.push(data.ingredients[i].name);
						this.dosages.push(data.ingredients[i].dosage);
					}
					this.quill.setText('');
					this.quill.clipboard.dangerouslyPasteHTML(0,this.text,);
					this.pic_path = data.main_pic;
				},
				error: function(data){
					console.log(data);
				},
			});
		},
	},
	mounted: function(){
		component = this;
		var toolbarOptions = ['bold', 'link', 'italic', 'underline', 'strike', 'video', 'image', 'align','blockquote'];
		this.quill = new Quill('#editor', {
			modules: {
			  toolbar: '#toolbar',
			},
			theme: 'snow',
		});
		this.$root.$on('get_pic_path', function(path){
			component.pic_path = path;
		});
		
		this.$root.$on('upload_success', function(){
			component.$root.$emit('update_img');
		});
	},
	template: 	`<div class="add_change">
					<div class="search_wrapp">
						<input v-model="id" type="text" placeholder="id">
						<div class="btn" @click="search_recipe">Найти</div>
					</div>
					<input v-model="title" type="text" id="title" placeholder="Заголовок">
					<div id="ingredients">
						<div id = "wrap_ingr">
							<div class="ingredient" v-for="(ingredient, index) in ingredients">
								<input v-bind:id="'ingr' + index" type="text" class="name" v-on:input="change_num_of_ingr" placeholder="название ингредиента" :value="ingredient">
								<div class = "dropdown" v-show="ingredients_search!=null && current_dropdown == index">
									<div class="ingredient" v-for= "ingredient in ingredients_search" @click ="select_ingr" >{{ingredient.name}}</div>
								</div>
							</div> 
						</div>
						<div id = "wrap_dosage">
						<div class="dosage" v-for="(dosage, index) in dosages">
							<input v-bind:id="'dosage' + index" type="text" class="dosage" v-on:input="change_num_of_ingr" placeholder="количество" :value="dosage">
						</div> 
						</div>
					</div>
					<img id="main_img" v-show="pic_path!=null" :src="site +'/'+pic_path">
					<upload_img></upload_img>
					<img_picker></img_picker>
					<div id="toolbar">
					  <select class="ql-size">
						<option value="10px">Small</option>
						<option value="13px" selected>Normal</option>
						<option value="18px">Large</option>
						<option value="32px">Huge</option>
					  </select>
					  <span class="ql-formats">
						<button class="ql-align" value=""></button>
						<button class="ql-align" value="center"></button>
						<button class="ql-align" value="right"></button>
						<button class="ql-align" value="justify"></button>
					  </span>
					  <button class="ql-bold"></button>
					  <button class="ql-italic"></button>
					  <button class="ql-strike"></button>
					  <button class="ql-underline"></button>
					  <button class="ql-link"></button>
					  <button class="ql-image"></button>
					</div>
					<div id="editor_wrapper">
						<div id="toolbar"></div>
						<div id="editor"></div>
					</div>
					<div class="btn_add" @click = "change_recipe">Изменить</div>
					{{message}}
				</div>`,
});

Vue.component('delete_recipe', {
	data: function(){
		return {
			recipe_id: null,
		}
	},
	methods: {
		del_recipe: function(){
			component = this;
			$.ajax({
				type: 'DELETE',
				url: site + '/recipes/' + this.recipe_id,
				context: component,
				headers:{
					Authorization: "Bearer " + localStorage.getItem('token'),
				},
				success: function(data){
					this.recipe_id = null;
				},
			});
		},
	},
	template: 	`<div class="del">
					<div class="title">Удалить рецепт</div>
					<input v-model="recipe_id" type="text" placeholder="id">
					<div class="btn" @click="del_recipe">Удалить</div>
				</div>`,
});

Vue.component('manage_ingredients', {
	data: function(){
		return {
			all_ingr: null,
			selected_ingr: null,
			ingr_for_add: null,
		}
	},
	methods: {
		get_all_ingr: function(){
			component = this;
			$.ajax({
					type: 'GET',
					url: site + '/ingredients',
					context: component,
					success: function(data){
						this.all_ingr = data;
					},
				});
		},
		delete_ingr: function(){
			$.ajax({
				type: 'GET',
				url: site + '/ingredients/get-id?name=' + this.selected_ingr,
				context: component,
				success: function(data){
					$.ajax({
						type: 'DELETE',
						url: site + '/ingredients/' + data.id,
						context: component,
						headers:{
							Authorization: "Bearer " + localStorage.getItem('token'),
						},
						success: function(data){
							this.get_all_ingr();
						},
					});
				},
			});
		},
		add_ingr: function(){
			component = this;
			$.ajax({
				type: 'POST',
				url: site + '/ingredients',
				context: component,
				headers:{
					Authorization: "Bearer " + localStorage.getItem('token'),
				},
				data: {name: this.ingr_for_add},
				success: function(data){
					this.ingr_for_add = null;
					this.get_all_ingr();
				},
			});
		},
	},
	mounted: function(){
		this.get_all_ingr();
	},
	template: 	`<div class="manage">
					<div id="add_ingr">
						<div class="title">Добавить</div>
						<input v-model="ingr_for_add" type="text" placeholder="Название">
						<div class="btn" @click="add_ingr">Добавить</div>
					</div>
					<div id="del_ingr">
						<div class="title">Удалить</div>
						<select v-model="selected_ingr">
						  <option v-for="ingredient in all_ingr" :value="ingredient.name">{{ingredient.name}}</option>
						</select>
						<div class="btn" @click="delete_ingr">Удалить</div>
					</div>
				</div>`,
});

Vue.component('upload_img', {
	data: function(){
		return {
			url: site + '/site/upload-img',
			img: null,
			message: null,
		}
	},
	methods: {
		upload: function(){
			component = this;
			var fd = new FormData;
			fd.append('img', this.img);
			$.ajax({
				type: 'POST',
				url: this.url,
				context: component,
				data: fd,
				processData: false,
				contentType: false,
				success: function(data){
					this.message = "Файл загружен";
					this.$root.$emit('upload_success');
				},
				error: function(data){
					this.message = "Произошла ошибка";
				},
			});
		},
		change_upload: function(){
			this.img = event.target.files[0];
		},
	},
	template: 	`<div class="upload_img">
					<input type="file" name="img" @change="change_upload">
					<div class="btn" @click="upload"> Загрузить</div>
					<div class="msg">{{message}}</div>
				</div>`,
});

Vue.component('img_picker', {
	data: function(){
		return {
			url: site + '/site/get-all-files',
			pic_url: site + '/pic/',
			current_img: null,
			names: null,
		}
	},
	methods: {
		get_all_files: function(){
			component = this;
			var fd = new FormData;
			fd.append('img', this.img);
			$.ajax({
				type: 'GET',
				url: this.url,
				context: component,
				success: function(data){
					this.names = JSON.parse(data);
				},
				error: function(data){
					console.log(data);
				},
			});
		},
		pick: function(data){
			this.current_img = 'pic/' + data.target.dataset.name;
			this.$root.$emit('get_pic_path', this.current_img);
		},
	},
	mounted: function(){
		self = this;
		this.get_all_files();
		this.$root.$on('update_img', function(){
			self.get_all_files();
		});
	},
	template: 	`<div class="img_picker">
					<div class="img_container">
						<img v-for="name in names" :src="pic_url + name" :data-name="name" @click="pick">
					</div>
				</div>`,
});

Vue.component('progress_bar', {
	data: function(){
		return {
		}
	},
	methods: {
	},
	template: 	`<div id="progress"></div>`,
});

var router = new VueRouter({
	mode: 'history',
	routes: [
		{path: '/:id', component: mainPage},
		{path: '/', component: mainPage},
		{path: '/page/allrecipes/:page', component: allRecipes},
		{path: '/page/login', component: login},
		{path: '/page/admin', component: admin}
	]
});


var app = new Vue({ //todo -(наполнение контентом), сео.
	el: '#app',
	router,
});

$( document ).ready(function() {
});


