<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
		'https://cdn.quilljs.com/1.3.6/quill.snow.css',
        'css/site.css',
    ];
    public $js = [
		'js/vue.min.js',
		'js/jquery-3.3.1.min.js',
		'js/vue-router.min.js',
		'https://cdn.quilljs.com/1.3.6/quill.min.js',
		'js/blowup.js',
		'js/main.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
