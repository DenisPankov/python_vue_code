<?php

/* @var $this yii\web\View */

$this->title = Yii::$app->name;
?>

<div id = 'app'>
	<nav>
		 <router-link to="/">Главная</router-link>
		 <router-link to="/page/allrecipes/1">Все рецепты</router-link>
	</nav>
	<router-view></router-view>
</div>
