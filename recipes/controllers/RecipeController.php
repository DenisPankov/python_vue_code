<?php
namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use app\models\Recipe;
use app\models\IngredientsInRecipe;
use app\models\Ingredient;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\AccessControl;

class RecipeController extends ActiveController
{
    public $modelClass = 'app\models\Recipe';
	
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => HttpBearerAuth::className(), // задал аунтефикацию через OAuth для некоторых экшенов
			'only'=> ['create', 'delete', 'update'],
		];
		$behaviors['access'] = [ //запретил неаунтефицированным пользователям редактировать, удалять, создавать записи в бд
			'class' => AccessControl::className(),
            'rules' => [
				[
                    'allow' => true,
                    'actions' => ['search', 'search-by-id', 'view', 'index', 'options',],
                    'roles' => ['?'],
                ],
                [
                    'allow' => true,
                    'actions' => ['search', 'search-by-id', 'view', 'index', 'options','create', 'delete', 'update'],
                    'roles' => ['@'],
                ],
            ],
		];
		return $behaviors;
	}
	
	public function actions(){
		$actions = parent::actions();
		unset($actions['create']);  //убрал дефотную реализацию и заменил своей
		unset($actions['update']);
		return $actions;
	}
	
	public function actionCreate(){ // создает рецепт и связанные с ними записи в промежуточной и таблице ингредиентов
		$recipe = new Recipe();// должны приходить поля title, text и номерные ingridient1, ingredient2 ..., dosage1, dosage2, ...
		$recipe->text = Yii::$app->request->post()['text'];
		$recipe->title = Yii::$app->request->post()['title'];
		$recipe->main_pic = Yii::$app->request->post()['main_pic'];
		$ingredients = array();
		$dosage = array();
		foreach (Yii::$app->request->post() as $key => $value){
			if(substr($key,0,-1) == 'ingredient'){
				array_push($ingredients,$value);
			}
			if(substr($key,0,-1) == 'dosage'){
				array_push($dosage,$value);
			} 
		}
		$recipe->save();
		for( $i = 0; $i < count($ingredients); $i++){
			$recipe->link('ingredients', Ingredient::find()->where(['name' => $ingredients[$i]])->one(), ['dosage' => $dosage[$i]]);
		}
		return 200;
	}
	
	public function actionUpdate(){
		$recipe = Recipe::find()->where(['id' => Yii::$app->request->post()['id']])->one();
		$recipe->text = Yii::$app->request->post()['text'];
		$recipe->title = Yii::$app->request->post()['title'];
		$recipe->main_pic = Yii::$app->request->post()['main_pic'];
		$ingredients = Yii::$app->request->post()['ingredients'];
		$dosages = Yii::$app->request->post()['dosage'];
		$recipe->save();
		for( $i = 0; $i < count($ingredients); $i++){
			$recipe->unlink('ingredients', Ingredient::find()->where(['name' => $ingredients[$i]])->one(), true);
		}
		for( $i = 0; $i < count($ingredients); $i++){
			$recipe->link('ingredients', Ingredient::find()->where(['name' => $ingredients[$i]])->one(), ['dosage' => $dosages[$i]]);
		}
		return 200;
	}
	
	public function actionSearch(){ //ищет по ингрединтам
		return $recipes = Yii::$app->db->createCommand("SELECT recipe.id, recipe.title, recipe.main_pic, GROUP_CONCAT(DISTINCT ingredient.name ORDER BY recipe.id ASC SEPARATOR ',') AS ingredients 
														FROM recipe, ingredient, ingredients_in_recipe
														WHERE recipe.id = ingredients_in_recipe.id_recipe 
															AND ingredient.id = ingredients_in_recipe.id_ingredient
															AND ingredient.name IN ".  '(\'' . implode('\',\'', Yii::$app->request->get()) ."')
														GROUP BY recipe.id")
            ->queryAll();
	}
	
	public function actionSearchById($id){ //ищет по id и возвращает с ингредиентами
		$recipe = Yii::$app->db->createCommand("SELECT recipe.title , recipe.text, recipe.main_pic, ingredient.name, ingredients_in_recipe.dosage
														FROM recipe, ingredient, ingredients_in_recipe
														WHERE  recipe.id =" . $id . " 
															AND recipe.id = ingredients_in_recipe.id_recipe 
															AND ingredient.id = ingredients_in_recipe.id_ingredient
														")
            ->queryAll();
		$ingredients = array();
		foreach ($recipe as $element){
			
			array_push($ingredients, ['name' => $element['name'], 'dosage' => $element['dosage']]);
			
		}
		$response_recipe = array('title' => $recipe[0]['title'], 'text' => $recipe[0]['text'], 'main_pic' => $recipe[0]['main_pic'], 'ingredients' => $ingredients);
		return $response_recipe;
	}
}