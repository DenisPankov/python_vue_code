<?php

namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use app\model\User;

class UserController extends ActiveController{
	
	public $modelClass = 'app\models\User';
	
	public function actions(){
		$actions = parent::actions();
		unset($actions['create']);
		unset($actions['delete']);
		unset($actions['view']);
		unset($actions['index']);
		unset($actions['update']);
		return $actions; // убрал реализации методов для упоавления пользвателями. правильнее было бы разграницить права
	}
	
	public function actionLogin(){
		// метод написан неправильно. нужно проверять, вернул ли sql запрос имя и если токен = 0, то генерировать новый
		// или, если не равен, то возвращать его. если имя не найдено, нужно вернуть соответствующее сообщение
		$username = Yii::$app->request->post()['username'];
		$pass = Yii::$app->request->post()['pass'];
		$oldToken = Yii::$app->db->createCommand("SELECT token, name FROM user WHERE name = '" . $username . "' AND pass= '" . $pass . "'")
            ->queryAll();
		if(count($oldToken) == 0){
			return $oldToken;
		}else{
			$token = bin2hex(openssl_random_pseudo_bytes(16));
			Yii::$app->db->createCommand("UPDATE user SET token = '".$token."' WHERE name = '" . $username . "' AND pass= '" . $pass . "'")
            ->execute();
			return Yii::$app->db->createCommand("SELECT token, name FROM user WHERE name = '" . $username . "' AND pass= '" . $pass . "'")
            ->queryAll();
		}
	}
	
	public function actionCheck(){// проверяет, является ли пользователь зарегестрированным и админом
		$token = Yii::$app->request->post()['token'];
		$result = Yii::$app->db->createCommand("SELECT name FROM user WHERE token = '" . $token ."'")
            ->queryAll();
		if (count($result)!=0){
			if($result[0]['name'] == 'admin')
				return "200";
			else
				return "403";
		}else{
			return "403";
		}
	}
}