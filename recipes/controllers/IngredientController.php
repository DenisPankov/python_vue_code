<?php
namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\AccessControl;
use yii\filters\Cors;
use yii\data\ActiveDataProvider;
use app\models\Ingredient;

class IngredientController extends ActiveController
{
    public $modelClass = 'app\models\Ingredient';
	
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => HttpBearerAuth::className(),
			'only'=> ['create', 'delete', 'update'],
		];
		$behaviors['access'] = [
			'class' => AccessControl::className(),
            'rules' => [
				[
                    'allow' => true,
                    'actions' => ['search', 'view', 'index', 'options', 'get-id'],
                    'roles' => ['?'],
                ],
                [
                    'allow' => true,
                    'actions' => ['search', 'view', 'index', 'options', 'create', 'delete', 'update', 'get-id'],
                    'roles' => ['@'],
                ],
            ],
		];
		return $behaviors;
	}
	
	public function actions(){
        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }
	
	public function actionIndex(){ // переопределил метод index, чтобы возвращались все ингредиенты сразу, а не страницами
        return new ActiveDataProvider([
            'query' => Ingredient::find(),
            'pagination' => false,
        ]);
    }
	
	public function actionSearch($name){
		return $searchResult = Yii::$app->db->createCommand("SELECT name FROM ingredient WHERE name LIKE '%".$name."%'")
            ->queryAll();
	}
	
	public function actionGetId($name){
		return $searchResult = Yii::$app->db->createCommand("SELECT id FROM ingredient WHERE name= '".$name."'")
            ->queryOne();
	}
}

