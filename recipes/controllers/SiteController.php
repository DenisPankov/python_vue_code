<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
	
	public function actionUploadImg(){
		$date = date_create();
		move_uploaded_file($_FILES['img']['tmp_name'], 'pic/'.date_timestamp_get($date).'.jpg');
		// загружает изображение на сервер и переименевывает его
	}
	
	public function actionGetAllFiles(){
		$files = scandir('pic/', 1);
		unset($files[count($files)-1]);
		unset($files[count($files)-1]);
		return json_encode($files); // возвращает адрес изображений
	}
    
}
