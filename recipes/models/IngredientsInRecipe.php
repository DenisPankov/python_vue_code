<?php
namespace app\models;

use yii\db\ActiveRecord;

class IngredientsInRecipe extends ActiveRecord{
	
	public static function tableName()
    {
        return '{{ingredients_in_recipe}}';
    }
}

?>