<?php
namespace app\models;

use yii\db\ActiveRecord;

class Ingredient extends ActiveRecord{
	
	public function fields(){
		
		return ['name'];
	} 
	
	public static function tableName()
    {
        return '{{ingredient}}';
    }
	
	public function rules() // поле должно быть передано обязательно при пост запросе(на добавление)
	{
		return [
			[['name'], 'required'],
		];
	}
	
}

