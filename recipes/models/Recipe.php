<?php
namespace app\models;

use yii\db\ActiveRecord;
use app\models\IngredientsInRecipe;
use app\models\Ingredient;

class Recipe extends ActiveRecord{
	
	public function fields(){
		return ['id', 'title', 'main_pic'];
	} 
	
	public static function tableName(){
        return '{{recipe}}';
    }

	public function getIngredients(){
		
		return $this->hasMany(Ingredient::className(), ['id' => 'id_ingredient'])
							->viaTable('ingredients_in_recipe', ['id_recipe' => 'id']);
		
	}
}
