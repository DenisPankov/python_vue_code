<?php
namespace app\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface
{
    public static function findIdentityByAccessToken($token, $type = null)
    {
		/*
		чтобы отправить авторизованный запрос нужно к заголовку добавить Bearer *токен из бд*. 
		ключ массива = поле в бд. Доступ проверяется в одном из событий до выполнения действия контроллера.
		для каждого контроллера нужно указывать метод аутнтификации.
		*/
        return static::findOne(['token' => $token]);
    }
	
	public function getId(){
		
	}
	public static function findIdentity($id){
		
	}
	public function getAuthKey(){
		
	}
	
	public function validateAuthKey($authKey){
		
	}
}