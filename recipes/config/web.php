<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
	'name' => 'Простые рецепты',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '325423542345623642346',
			'enableCsrfValidation' => false,
			'parsers' => [
			  'application/json' => 'yii\web\JsonParser',
			],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
			'enableSession' => false,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error' //изменил с error на index(errors), чтобы сервер перенаправлял при ненахождении страницы на главную
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
			'enableStrictParsing' => false,
            'rules' => [
				'POST site/upload-img' => 'site/upload-img',
				'GET site/get-all-files' => 'site/get-all-files',
				'GET site/get-ip' => 'site/get-ip',
				'GET /<id:\d+>' => 'site/index',
				'GET page/login' => 'site/index',
				'GET page/admin' => 'site/index',
				'GET page/allrecipes/<id:\d+>' => 'site/index',
				['class' => 'yii\rest\UrlRule', 'controller' => ['ingredient'],'extraPatterns' => [
																			'GET search' => 'search',
																			'GET get-id' => 'get-id']
				],
				['class' => 'yii\rest\UrlRule', 'controller' => ['recipe'],'extraPatterns' => [
																			'GET search' => 'search',
																			'GET search-by-id' => 'search-by-id']
				],
				['class' => 'yii\rest\UrlRule', 'controller' => ['user'],'extraPatterns' =>[
																			'POST login' => 'login',
																			'POST check' => 'check']
				],
			],
		],	
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
